<?php

use Illuminate\Database\Seeder;
use App\Club;

class ClubTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $club = new club();
        $club->name_club = 'Manchester United';
        $club->coach = 'Ole Gunner Solskjaer';
        $club->logo = 'club-image/MU.png';
        $club->club_value = '900000000';
        $club->save();

        $club = new club();
        $club->name_club = 'Real Madrid';
        $club->coach = 'Zinédine Zidane';
        $club->logo = 'club-image/real.png';
        $club->club_value = '600000000';
        $club->save();
    }
}
