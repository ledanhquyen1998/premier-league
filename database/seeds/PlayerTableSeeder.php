<?php

use Illuminate\Database\Seeder;
use App\Player;

class PlayerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $player = new Player();
        $player->name_player = 'Wayne Rooney';
        $player->country = 'England';
        $player->attack = '98';
        $player->defferend = '98';
        $player->height = '176';
        $player->weight = '81';
        $player->club_id = 1;
        $player->player_value = '75000000';
        $player->image = 'player-image/rooney.jpg';
        $player->save();

        $player = new Player();
        $player->name_player = 'Cristiano Ronaldo';
        $player->country = 'Portugal';
        $player->attack = '98';
        $player->defferend = '70';
        $player->height = '185';
        $player->club_id = 2;
        $player->weight = '82';
        $player->player_value = '75000000';
        $player->image = 'player-image/ronaldo.jpg';
        $player->save();
    }
}
