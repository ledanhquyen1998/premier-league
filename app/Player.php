<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
	protected $table = 'players';
	protected $fillable = [
		'name_player','attack','defferend','image','player_value','club_id','country','height','weight'
	];

	public function club()
	{
		return $this->belongsTo('App\Club');
	}
}
