<?php


namespace App\Http\Repository\eloquent;


use App\Http\Repository\PlayerRepositoryInterface;
use App\Player;

class PlayerRepository extends BaseRepository implements PlayerRepositoryInterface
{
    protected $player;
    protected $playerRepository;

    public function __construct(Player $player,
                                PlayerRepositoryInterface $playerRepository)
    {
        $this->player = $player;
        $this->playerRepository = $playerRepository;
    }

    public function getAll()
    {
        return $this->player->all();
    }

    public function add($data)
    {
        return $data->save();
    }

    public function update($data)
    {
        return $data->save();
    }

    public function delete($data)
    {
        return $data->delete();
    }

    public function show($id)
    {
        return $this->player->findOrFail($id);
    }
}
