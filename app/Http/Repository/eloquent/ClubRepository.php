<?php


namespace App\Http\Repository\eloquent;


use App\Club;
use App\Http\Repository\ClubRepositoryInterface;

class ClubRepository extends BaseRepository implements ClubRepositoryInterface
{
    protected $club;
    public function __construct(Club $club)
    {
        $this->club = $club;
    }

    public function getAll()
    {
        return $this->club->all();
    }

    public function add($data)
    {
        return $data->save();
    }

    public function update($data)
    {
        return $data->save();
    }

    public function delete($data)
    {
        return $data->delete();
    }

    public function show($id)
    {
        return $this->club->findOrFail($id);
    }
}
