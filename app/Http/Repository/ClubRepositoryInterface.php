<?php


namespace App\Http\Repository;


interface ClubRepositoryInterface
{
    public function getAll();

    public function add($data);

    public function update($data);

    public function delete($data);

    public function show($id);
}
