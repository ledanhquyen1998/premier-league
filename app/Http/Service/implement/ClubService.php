<?php


namespace App\Http\Service\implement;


use App\Club;
use App\Http\Repository\ClubRepositoryInterface;
use App\Http\Service\ClubServiceInterface;

class ClubService extends BaseService implements ClubServiceInterface
{
    protected $clubRepository;
    protected $club;

    public function __construct(ClubRepositoryInterface $clubRepository,
                                Club $club)
    {
        $this->clubRepository = $clubRepository;
        $this->club = $club;
    }

    public function getAll()
    {
        return $this->clubRepository->getAll();
    }

    public function add($request)
    {
        $data = $this->club;
        $data->name_club = $request->name_club;
        $data->coach = $request->coach;
        $data->logo = $request->logo;
        $data->club_valud = $request->club_value;
        return $this->clubRepository->add($data);
    }

    public function update($request, $id)
    {
        $data = $this->club->findOrFail($id);
        $data->name_club = $request->name_club;
        $data->coach = $request->coach;
        $data->logo = $request->logo;
        $data->club_valud = $request->club_value;
        return $this->clubRepository->update($data);
    }

    public function delete($id)
    {
        $data = $this->club->findOrFail($id);
        return $this->clubRepository->delete($data);
    }

    public function show($id)
    {
        return $this->clubRepository->show($id);
    }
}
