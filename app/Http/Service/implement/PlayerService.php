<?php


namespace App\Http\Service\implement;


use App\Http\Repository\PlayerRepositoryInterface;
use App\Http\Service\PlayerServiceInterface;
use App\Player;

class PlayerService extends BaseService implements PlayerServiceInterface
{
    protected $player;
    protected $playerRepository;

    public function __construct(Player $player,
                                PlayerRepositoryInterface $playerRepository)
    {
        $this->player = $player;
        $this->playerRepository = $playerRepository;
    }

    public function getAll()
    {
        return $this->playerRepository->getAll();
    }

    public function add($request)
    {
        $data = $this->player;
        $data->name_player = $request->name_player;
        $data->country = $request->country;
        $data->attack = $request->attack;
        $data->defferend = $request->defferend;
        $data->height = $request->height;
        $data->weight = $request->weight;
        $data->player_value = $request->player_value;
        $data->image = $request->image;
        $data->club_id = $request->club_id;
        return $this->playerRepository->add($data);
    }

    public function update($request, $id)
    {
        $data = $this->playerRepository->show($id);
        $data->name_player = $request->name_player;
        $data->country = $request->country;
        $data->attack = $request->attack;
        $data->defferend = $request->defferend;
        $data->height = $request->height;
        $data->weight = $request->weight;
        $data->player_value = $request->player_value;
        $data->image = $request->image;
        $data->club_id = $request->club_id;
        return $this->playerRepository->update($data);

    }

    public function delete($id)
    {
        $data = $this->playerRepository->show($id);
        return $this->playerRepository->delete($data);
    }

    public function show($id)
    {
        return $this->playerRepository->show($id);
    }
}
