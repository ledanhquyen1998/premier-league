<?php


namespace App\Http\Service;


interface ClubServiceInterface
{
    public function getAll();

    public function add($request);

    public function update($request, $id);

    public function delete($id);

    public function show($id);
}
