<?php


namespace App\Http\Service;


interface PlayerServiceInterface
{
    public function getAll();

    public function add($request);

    public function update($request, $id);

    public function delete($id);

    public function show($id);
}
