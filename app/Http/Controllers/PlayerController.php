<?php

namespace App\Http\Controllers;

use App\Http\Service\PlayerServiceInterface;
use App\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    protected $player;
    protected $playerService;

    public function __construct(Player $player,
                                PlayerServiceInterface $playerService)
    {
        $this->player = $player;
        $this->playerService = $playerService;
    }

    public function index()
    {
        $data['players'] = $this->playerService->getAll();
        return view('player.list');
    }

    public function create()
    {
        return view('player.create');
    }

    public function store(Request $request)
    {
        return $this->playerService->add($request);
    }

    public function show($id)
    {
        $data['player'] = $this->playerService->show($id);
        return view('player.total', $data);
    }

    public function edit($id)
    {
        $data['player'] = $this->playerService->show($id);
        return view('player.edit', $data);
    }

    public function update(Request $request, $id)
    {
        return $this->playerService->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->playerService->delete($id);
    }
}
