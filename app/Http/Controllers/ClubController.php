<?php

namespace App\Http\Controllers;

use App\Http\Service\ClubServiceInterface;
use Illuminate\Http\Request;
use App\Club;

class ClubController extends Controller
{
    protected $club;
    protected $clubService;

    public function __construct(Club $club,
                                ClubServiceInterface $clubService)
    {
        $this->club = $club;
        $this->clubService = $clubService;
    }

    public function index()
    {
        $data['clubs'] = $this->clubService->getAll();
    }

    public function create()
    {
        return view('club.create');
    }

    public function store(Request $request)
    {
        $this->clubService->add($request);
    }

    public function show($id)
    {
        $data['club'] = $this->clubService->show($id);
        return view('club.total', $data);
    }

    public function edit($id)
    {
        $data['club'] = $this->clubService->show($id);
        return view('club.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->clubService->update($request, $id);
    }

    public function destroy($id)
    {
        $this->clubService->delete($id);
    }
}
