<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $table = 'clubs';
    protected $fillable = [
    	'name_club','coach','logo','club_value'
    ];

    public function players()
    {
    	return $this->hasMany('App\Player');
    }
}
