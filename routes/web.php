<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('/clubs')->group(function () {
    Route::get('/list-club', 'ClubController@index')->name('club.list');
    Route::get('/create-new-club', 'ClubController@create')->name('club.create');
    Route::post('/store', 'ClubController@store')->name('club.store');
    Route::get('/edit-club', 'ClubController@edit')->name('club.edit');
    Route::post('/update-club/{id}', 'ClubController@update')->name('club.update');
    Route::get('/total-club/{id}', 'ClubController@show')->name('club.show');
});

Route::prefix('/players')->group(function () {
    Route::get('/list-player', 'PlayerController@index')->name('player.list');
    Route::get('/create-new-player', 'PlayerController@create')->name('player.create');
    Route::post('/store', 'PlayerController@store')->name('player.store');
    Route::get('/edit-player', 'PlayerController@edit')->name('player.edit');
});
